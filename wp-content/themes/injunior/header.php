
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?php
        if(is_front_page()){
            echo wp_title('');
        } elseif (is_page()){
            echo wp_title('');
            echo ' - ';
        } elseif (is_search()){
            echo 'Busca - ';
        } elseif (!(is_404()) && (is_single()) && (is_page())){
            echo wp_title('');
            echo ' - ';
        } elseif (is_404()){
            echo 'Não encontrada';
        } bloginfo('name');
        
        ?>
    </title>
    <link rel="icon" href="assets/img/logo-in-junior.png">
    <?php wp_head() ?>

</head>
<body>
    <header>
        <nav class="container">
            <a href="<?php if(is_front_page()){echo "#";} else{echo get_home_url();} ?>"><img src="<?php echo get_template_directory_uri() ?>/assets/img/logoIN.svg" alt="logo"></a>
            <input type="checkbox" id="checkconfig" style="display: none"/>
            <label for="checkconfig" class="nav-icon">
                <div></div>
            </label>

            <?php 
              $args = array(
                'menu' => 'navegacao', 
                'container' => true
              );
              wp_nav_menu($args);
            ?>
            <!-- <ul id="menu-navegacao" class="menu">
                <li ><a href="/servicos.html">Serviços</a></li>
                <li ><a href="/noticias.html">Notícias</a></li>
                <li ><a href="/contato.html">Contato</a></li>
            </ul> -->
        </nav>
    </header>