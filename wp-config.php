<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Y/d61pKUoArtcG2Ox7OPCCc/DolALBIc3McYWBKRphvJkNI/0YTbCUyIw0MB9sSTn5r8StmZGi4404RrTjcswg==');
define('SECURE_AUTH_KEY',  'gOH+wdwshFcqr/+cw+ojAd/XVZyK6fvXz5rPluOeHXJDttf70guNkY+Pm+3/eX2cc/slBwb8N43p/M/cMfkbBA==');
define('LOGGED_IN_KEY',    'F17gPjoHhsgeSggiZWNx2+NWyyutnXbLj+J02c5xpBoipl7EDny8hNgoRQIRGD8CJBH3GPNCIYeLxU8WwtSd6Q==');
define('NONCE_KEY',        'CgmAiYEYeA2I06cjm2dW8J5vJv6cvkcUor0y1RHXH8amGfuBrecYvJIfN6gMhVYGUQYg8z4Aq5Ur2WYhZV0YXQ==');
define('AUTH_SALT',        'Ar+hvhGjspCLXSsh4Qae0nh1w58gkQprC6oR4/uvez1XpykG6KXsXoo954LwKfrTzX3BzEjBlBQJmZmvo4C+Pw==');
define('SECURE_AUTH_SALT', 'hNzveEFuKvh1E0cvUmXCgBVLQUWBXXArJyws1TLvUikK3A+eoDQszU5oGcsBKuN5RPORq7zzLCrQBFVnqzsbUw==');
define('LOGGED_IN_SALT',   'XrOKOJIOqChcLoyHbKbPtjs/enRQQ1YZSIFm6gQOEgMu70A44ALKlESQOhshj/nZvlRNRu3Zpx09Mx7XH2F5LQ==');
define('NONCE_SALT',       'YL+J3XWqo8gxdlp5K/eN3edUCEGePCfgzNaOCrpKJa63styE59fS1ccgPaq4Fn6Jt6c7YMo1aFbzWNlFTxOq4A==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
